This Dockerfile is to build a Ubuntu-16.04-based container, used for cross-compiling Python 3 targeting Ubuntu ARM.

## Use case

I have a BeagleBone Black board running Ubuntu 16.04. I'm developing software for this board and want to base my software on Python 3.6, which is not available in Ubuntu 16.04 repository.

Building Python 3.6 directly on the board is possible, but it takes quite long time, due to ARM CPU's weakness.

### Why not do cross-compile directly on personal computer (without Docker container)?

My laptop is running Ubuntu 17.04. Its GCC and other libraries have different version from the ones in BeagleBone's Ubuntu. The produced binaries may have compatibility issue.

### Why not create a virtual machine (by VirtualBox) and do cross-compile there?

I don't want to waste my modest laptop CPU (only i3) for virtualization layer.

## How to cross compile with this Docker container

### Build container

Assume that you clone this Git repo to _~/docker-cross-compiler-armhf-ubuntu-xenial_. Before building container, you may want to tweak the _Dockerfile_ a bit. Because I live in Viet Nam, I set APT repositories in _Dockerfile_ with Viet Nam and Taiwan mirrors for fast download. If you are outside Viet Nam, you should avoid Viet Nam mirror and pick other.

```console
$ cd ~/docker-cross-compiler-armhf-ubuntu-xenial
$ docker build . -t docker-cross-compiler-armhf-ubuntu-xenial
```

The container has:

 - gcc 5
 - arm-linux-gnueabihf-gcc-5 toolchain
 - llvm, needed for optimizing Python
 - Dependency libraries for Python, except Tk. I don't want to include Tk because I only make non-GUI software.

### Cross compile

This instruction is aware of [Missing extensions modules](https://bugs.python.org/issue28444) bug. So it consists of several steps, and involve many folders. Bear it with me :sweat:

Create folder _~/Py36_ to mount to container later. Put Python source to this folder and add other subfolders as following:

```
Py36
├── build-native
├── out-arm
├── out-native
└── Python-3.6.1
```

Create _config.site_ with content:

```
ac_cv_file__dev_ptmx=no
ac_cv_file__dev_ptc=no
```
and put to _Python-3.6.1_ folder.

Enter container:

```console
$ docker run --rm --volume /home/quan/Py36:/Py36 -ti docker-cross-compiler-armhf-ubuntu-xenial bash
```

From now on, the command which is issued in container will be prefixed with `[container]`. This is just convention, you won't see it in real console :wink:.

Build Python 3.6 native (x86)

```console
[container]$ cd /Py36/build-native
[container]$ /Py36/Python-3.6.1/configure --enable-shared --disable-ipv6 --prefix=/Py36/out-native
[container]$ make -j4
[container]$ make install
[container]$ cd /Py36/out-native/bin
[container]$ ln -s python3.6 python
[container]$ sudo ln -s /Py36/out-native/lib/libpython3.6m.so.1.0 /usr/local/lib/libpython3.6m.so.1.0
[container]$ sudo ldconfig
```

Build Python 3.6 for ARM

```console
[container]$ cd /Py36/Python-3.6.1
[container]$ PATH=/Py36/out-native/bin:$PATH CONFIG_SITE=config.site CC=arm-linux-gnueabihf-gcc-5 CXX=arm-linux-gnueabihf-g++-5 AR=arm-linux-gnueabihf-ar RANLIB=arm-linux-gnueabihf-ranlib READELF=arm-linux-gnueabihf-readelf ./configure --enable-shared --host=arm-linux --build=x86_64-linux-gnu --disable-ipv6 --enable-optimizations --prefix=/Py36/out-arm
[container]$ PATH=/Py36/out-native/bin:$PATH make -j4
[container]$ PATH=/Py36/out-native/bin:$PATH make -j4 altinstall
```
If the building is complete, the produced Python will be saved in _Py36/out-arm_. We exit Docker container and prepare to copy Python to BeagleBone board.

```console
$ cd ~/Py36/out-arm
$ tar czf ../usrlocal.tar.gz .
```

Copy to device. Given that you already configure SSH connection for BeagleBone in _~/.ssh/config_.

```console
$ scp ~/Py36/usrlocal.tar.gz beaglebone:/tmp/
$ ssh beaglebone
[beaglebone]$ cd /tmp/
[beaglebone]$ sudo tar xzf usrlocal.tar.gz -C /usr/local/
[beaglebone]$ sudo ldconfig
```

Done. Now you should already have Python 3.6 on your board.

## Known issues

- The compiled ARM Python is missing `pip`. You can install `pip` later in the board with this tutorial https://pip.pypa.io/en/stable/installing/.
- The step to profile Python for optimization can not be carried in a cross build yet.
